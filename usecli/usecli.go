package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
)

func main() {
	var greeting, name string

	app := cli.NewApp()
	app.Usage = "Get a friendly greeting."
	app.UsageText = "usecli [options]"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "name, n",
			Usage:       "the `NAME` of the greetee (required)",
			EnvVar:      "NAME",
			Destination: &name,
		},
		cli.StringFlag{
			Name:        "greeting, g",
			Usage:       "the `GREETING` to use",
			Value:       "Hello",
			EnvVar:      "GREETING",
			Destination: &greeting,
		},
	}
	app.HideHelp = true
	app.HideVersion = true
	app.Before = func(c *cli.Context) error {
		if name == "" {
			return cli.NewExitError("Name is required", 1)
		}
		return nil
	}
	app.Action = func(c *cli.Context) error {
		greet(greeting, name)
		return nil
	}
	app.Run(os.Args)
}

func greet(greeting, name string) {
	fmt.Printf("%s, %s!\n", greeting, name)
}
