package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	var name = flag.String("n", "", "who to greet, or use env var NAME")
	var greeting = flag.String("g", "", "how to greet, or use env var GREETING")
	flag.Parse()
	defaultFromEnvOrFail(name, "NAME")
	defaultFromEnvOrFail(greeting, "GREETING")

	fmt.Printf("%s, %s!\n", *greeting, *name)
}

func defaultFromEnvOrFail(ptr *string, varName string) {
	if *ptr == "" {
		var ok bool
		if *ptr, ok = os.LookupEnv(varName); !ok {
			fmt.Println(varName, "not set")
			flag.PrintDefaults()
			os.Exit(1)
		}
	}
}
